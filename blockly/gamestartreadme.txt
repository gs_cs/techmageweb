
RUNNING BLOCKLY
Assuming you have minecraft set up to receive blockly code...
blockly/apps/code/index.html is the homepage from which student run code.


ADDING BLOCKS
1. /apps/code/template.soy
register block to appear on page
Changes require running soy compiler.
    cd blockly/apps/code
    #command from top of template.soy
    java -jar ../_soy/SoyToJsSrcCompiler.jar --outputPathFormat generated/en.js --srcs ../common.soy,template.soy

2. /blocks/techmage.js
This is where "Language stub" from Block Factory goes. This is the block's visual definition.
Changes require running build.py.
    cd blockly
    python build.py

3. /generators/python/techmage.js
This is where "Generator stub" from Block Factory goes. This is the block's code definition.
Changes require running build.py.
    cd blockly
    python build.py


RUNNING TESTS - a minimal safety net
	cd blockly
	python gamestarttest.py
