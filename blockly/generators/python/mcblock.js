/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Python for logic blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.Python.mcblocks');

goog.require('Blockly.Python');


Blockly.Python['mcblock_ice'] = function(block) {
  return ['ICE',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_dirt'] = function(block) {
  return ['DIRT',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_tnt'] = function(block) {
  return ['TNT',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_gold_block'] = function(block) {
  return ['GOLD_BLOCK',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_glass'] = function(block) {
  return ['GLASS',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_air'] = function(block) {
  return ['AIR',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_iron_block'] = function(block) {
  return ['IRON_BLOCK',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_pumpkin'] = function(block) {
  return ['PUMPKIN',Blockly.Python.ORDER_NONE]
};

Blockly.Python['mcblock_stone'] = function(block) {
  return ['STONE',Blockly.Python.ORDER_NONE]
};
