/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Python for logic blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.Python.techmage');

goog.require('Blockly.Python');

var NEWLINE = '\n'


Blockly.Python['myX'] = function(block) {
  return ['myX()', Blockly.Python.ORDER_NONE];
};

Blockly.Python['myY'] = function(block) {
  return ['myY()', Blockly.Python.ORDER_NONE];
};

Blockly.Python['myZ'] = function(block) {
  return ['myZ()', Blockly.Python.ORDER_NONE];
};


Blockly.Python['setblock'] = function(block) {
  var x = Blockly.Python.valueToCode(block, 'x', Blockly.Python.ORDER_ATOMIC);
  var y = Blockly.Python.valueToCode(block, 'y', Blockly.Python.ORDER_ATOMIC);
  var z = Blockly.Python.valueToCode(block, 'z', Blockly.Python.ORDER_ATOMIC);
  var type = Blockly.Python.valueToCode(block, 'type', Blockly.Python.ORDER_ATOMIC);

  var code = 'setblock(' + x + ',' + y + ',' + z + ',' + type + ')' + NEWLINE;
  return code;
};

Blockly.Python['teleport'] = function(block) {
  var x = Blockly.Python.valueToCode(block, 'x', Blockly.Python.ORDER_ATOMIC);
  var y = Blockly.Python.valueToCode(block, 'y', Blockly.Python.ORDER_ATOMIC);
  var z = Blockly.Python.valueToCode(block, 'z', Blockly.Python.ORDER_ATOMIC);

  var code = 'teleport(' + x + ',' + y + ',' + z + ')' + NEWLINE;
  return code;
};

Blockly.Python['cube'] = function(block) {
  var x = Blockly.Python.valueToCode(block, 'x', Blockly.Python.ORDER_ATOMIC);
  var y = Blockly.Python.valueToCode(block, 'y', Blockly.Python.ORDER_ATOMIC);
  var z = Blockly.Python.valueToCode(block, 'z', Blockly.Python.ORDER_ATOMIC);
  var type = Blockly.Python.valueToCode(block, 'type', Blockly.Python.ORDER_ATOMIC);
  var size = Blockly.Python.valueToCode(block, 'size', Blockly.Python.ORDER_ATOMIC);

  var code = 'cube(' + x + ',' + y + ',' + z + ',' + type + ',' + size + ')' + NEWLINE;
  return code;
};



