/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Logic blocks for Blockly.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.Blocks.techmage');

goog.require('Blockly.Blocks');

var red = 0;
var blue = 230;
var MCBLOCK_TYPE = "mcblock"


Blockly.Blocks['setblock'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(red);
    this.appendDummyInput()
        .appendField("setblock");
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("x");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("z");
    this.appendValueInput("type")
        .setCheck(MCBLOCK_TYPE);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setInputsInline(true);
    this.setTooltip('');
  }
};


Blockly.Blocks['myX'] = {
  init: function() {
    this.setColour(blue);
    this.appendDummyInput()
        .appendField("myX");
    this.setOutput(true, "Number");
  }
};

Blockly.Blocks['myY'] = {
  init: function() {
    this.setColour(blue);
    this.appendDummyInput()
        .appendField("myY");
    this.setOutput(true, "Number");
  }
};

Blockly.Blocks['myZ'] = {
  init: function() {
    this.setColour(blue);
    this.appendDummyInput()
        .appendField("myZ");
    this.setOutput(true, "Number");
  }
};


Blockly.Blocks['teleport'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    
    this.setColour(red);
    this.appendDummyInput()
        .appendField("teleport");
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("x");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("z");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setInputsInline(true);
    this.setTooltip('');
  }
};

Blockly.Blocks['cube'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(red);
    this.appendDummyInput()
        .appendField("cube");
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("x");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("z");
    this.appendValueInput("type")
        .setCheck(MCBLOCK_TYPE);
    this.appendValueInput("size")
        .setCheck("Number")
        .appendField("size");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setInputsInline(true);
    this.setTooltip('');
  }
};


