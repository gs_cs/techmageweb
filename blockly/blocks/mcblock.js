/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Logic blocks for Blockly.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.Blocks.mcblocks');

goog.require('Blockly.Blocks');

var blue = 230
var imagePathCommon = "../../media/mcblock_";

Blockly.Blocks['mcblock_ice'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "ice.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("ICE");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_dirt'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "dirt.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("DIRT");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_gold_block'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "gold_block.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("GOLD BLOCK");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_tnt'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "tnt.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("TNT");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_glass'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "glass.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("GLASS");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_air'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "air.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("AIR");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_iron_block'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "iron_block.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("IRON_BLOCK");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_pumpkin'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "pumpkin.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("PUMPKIN");
    this.setOutput(true, "mcblock");
  }
};

Blockly.Blocks['mcblock_stone'] = {
  init: function() {
    this.setColour(blue);
    var imagePath = imagePathCommon + "stone.png"
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage(imagePath, 50, 50, "*"))
        .appendField("STONE");
    this.setOutput(true, "mcblock");
  }
};
