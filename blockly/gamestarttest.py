import unittest
import os

class gamestarttest(unittest.TestCase):

	def testExample(self):
		print 'this is only a test'
		assert 4 == 2 + 2

	def testTheseTestsAreRunningInAReasonableLocation(self):
		pathdirtblock = os.path.join('.', 'media', 'mcblock_dirt.png')
		assert os.path.isfile(pathdirtblock)
		
if __name__ == "__main__":
	print("begin gamestarttest")
	unittest.main()
