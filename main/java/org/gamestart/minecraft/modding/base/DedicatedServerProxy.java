package org.gamestart.minecraft.modding.base;

import org.gamestart.minecraft.modding.minecraft_python_programming.command.GameStartServerTickHandler;

import cpw.mods.fml.common.FMLCommonHandler;

public class DedicatedServerProxy extends CommonProxy {
	public DedicatedServerProxy(){
		FMLCommonHandler.instance().bus().register(new GameStartServerTickHandler());
	}
}
