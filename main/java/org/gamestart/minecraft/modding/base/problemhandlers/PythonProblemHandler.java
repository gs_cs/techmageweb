package org.gamestart.minecraft.modding.base.problemhandlers;

import net.minecraft.client.Minecraft;

import org.gamestart.minecraft.modding.base.MsgBox;

public class PythonProblemHandler extends AbstractProblemHandler {
	
	public static void printErrorMessageToDialogBox(Exception e) {
		String fullErrorMessage = getStackTraceFromException(e);
		String pythonErrorMessageOnly = fullErrorMessage.split("at org.")[0];
		if(ITS_FOR_THE_KIDS){
			MsgBox.error(pythonErrorMessageOnly, "Python Interpreter Failure - See Below For Error Message");
		}
		else{
			MsgBox.error(fullErrorMessage, "Python Interpreter Failure - See Below For Error Message");
		}
	}
}
