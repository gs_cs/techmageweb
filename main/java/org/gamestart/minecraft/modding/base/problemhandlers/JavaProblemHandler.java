package org.gamestart.minecraft.modding.base.problemhandlers;

import org.gamestart.minecraft.modding.base.MsgBox;

public class JavaProblemHandler extends AbstractProblemHandler {
	public static void printErrorMessageToDialogBox(Exception e){
		if(ITS_FOR_THE_KIDS){
			MsgBox.error(getStackTraceFromException(e), "Java/Minecraft Failure - This Is Not Your Fault - Grab An Instructor");
		}
		else{
			MsgBox.error(getStackTraceFromException(e), e.getMessage());
		}
	}
}
