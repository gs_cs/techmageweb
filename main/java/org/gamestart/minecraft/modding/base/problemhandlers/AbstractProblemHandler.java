package org.gamestart.minecraft.modding.base.problemhandlers;

import java.io.PrintWriter;
import java.io.StringWriter;

public class AbstractProblemHandler implements IProblemHandler {

	protected static boolean ITS_FOR_THE_KIDS = false;

	protected static String getStackTraceFromException(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		
		String fullErrorMessage = stringWriter.toString();
		return fullErrorMessage;
	}

}
