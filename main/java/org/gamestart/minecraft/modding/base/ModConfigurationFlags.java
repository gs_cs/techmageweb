package org.gamestart.minecraft.modding.base;

public class ModConfigurationFlags {
	public static boolean SPELLCRAFTERS(){return true;}
	public static boolean SPELLCRAFTERS_TERRAIN_GENERATION(){return false;}
	public static boolean SCAVENGER_HUNT(){return false;}
	public static boolean MINECRAFT_PYTHON_PROGRAMMING(){return true;}
	public static boolean STUDENT_MODS(){return false;}
}
