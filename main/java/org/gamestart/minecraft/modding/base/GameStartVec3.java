package org.gamestart.minecraft.modding.base;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;

public class GameStartVec3 {
		 public GameStartVec3(double x2, double y2, double z2) {
			 x=x2;
			 y=y2;
			 z=z2;
		 }
		 public GameStartVec3(EntityPlayer player) {
			Vec3 position = player.getPosition(1);
			x=position.xCoord;
			y=position.yCoord;
			z=position.zCoord;
		
		 }
		public double x;
		 public double y;
		 public double z;
		 
		 public int blockX(){return (int) Math.round(x);}
		 public int blockY(){return (int) Math.round(y);}
		 public int blockZ(){return (int) Math.round(z);}
}
