package org.gamestart.minecraft.modding.base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class JavaFileIOHelper {
	//The purpose of this guy right now is to abstract file I/O for Spellcrafters.
	//Long term, my hope is for him to cache spells - he will collaborate with a guy
	//that, given a file, will tell you if it has changed since the last answer - and help
	//optimize this so students' changes are immediate, but we're not loading from
	//disk every time they right click.
	
	public static JavaFileIOHelper SINGLETON = new JavaFileIOHelper();
	
	private JavaFileIOHelper(){
	}

	public String getTextContentOfFile(File file){
		try {
			return FileUtils.readFileToString(file);
		} catch (IOException e) {
			return "";
		}
}
}
