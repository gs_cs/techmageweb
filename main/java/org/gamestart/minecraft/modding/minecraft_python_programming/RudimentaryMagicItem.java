package org.gamestart.minecraft.modding.minecraft_python_programming;

import org.gamestart.minecraft.modding.minecraft_python_programming.spells.ISpell;
import org.gamestart.minecraft.modding.minecraft_python_programming.spells.SpellInterpreter;
import org.gamestart.minecraft.modding.minecraft_python_programming.spells.GameStartThreadFactory;

public class RudimentaryMagicItem implements IArcane {
	private ISpell storedSpell;
	public SpellInterpreter spellInterpreter;
	private long lastCast = 0;
	private int cooldown;

	public RudimentaryMagicItem(ISpell boundSpell, int cooldownInMillis) {
		storedSpell = boundSpell;
		this.cooldown = cooldownInMillis;
		this.spellInterpreter = new SpellInterpreter();
	}

	private long timer() {
		return System.currentTimeMillis() - lastCast;
	}

	@Override
	public void doMagic() {
		if (timer() > this.cooldown) {
			castStoredSpell();
			lastCast = System.currentTimeMillis();
		}
	}

	protected synchronized void castStoredSpell() {
		GameStartThreadFactory.makeSpellThread(this.storedSpell, spellInterpreter)
				.start();
	}
}
