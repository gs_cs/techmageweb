package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SpellExperienceLevelArbiter {

	public static int getRequiredExperienceLevelForSpell(ISpell spellForWand) {
		//you can ask the spell's name here and make changes
		return spellForWand.getRequiredExperienceLevel();
	}

}
