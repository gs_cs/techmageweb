package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.gamestart.minecraft.modding.minecraft_python_programming.web.GameStartHttpPostHandler;

public class JavaGameLoopRunnable implements Runnable {

	private SpellInterpreter spellInterpreter;

	public JavaGameLoopRunnable(SpellInterpreter spellInterpreter) {
		this.spellInterpreter = spellInterpreter;
	}

	@Override
	public void run() {

		try {
			ServerSocket server;
			server = new ServerSocket(57444, 10,
					InetAddress.getByName("127.0.0.1"));
			System.out.println("HTTP Server Waiting for client on port "
					+ server.getLocalPort());

			while (true) {
				Socket connected = server.accept();
				(new GameStartHttpPostHandler(connected, spellInterpreter))
						.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
