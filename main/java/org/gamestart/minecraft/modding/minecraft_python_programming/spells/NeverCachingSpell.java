package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;
import java.util.LinkedHashMap;

import org.gamestart.minecraft.modding.base.JavaFileIOHelper;
import org.python.core.PyCode;
import org.python.util.PythonInterpreter;


public class NeverCachingSpell extends AbstractSpell{

	
	public NeverCachingSpell(String name, File pythonScript) {
		super(name, pythonScript);
	}

	@Override
	public PyCode getCompiledPythonCode(PythonInterpreter interpreter) {
		compileSpell(interpreter);
		return this.pythonCompiledCode;
	
	}
}
