package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;

import org.python.core.PyCode;
import org.python.util.PythonInterpreter;

public class NullSpell extends AbstractSpell {

	public NullSpell(String name, File pythonScript) {
		super(name, pythonScript);
	}

	@Override
	public PyCode getCompiledPythonCode(PythonInterpreter interpreter) {
		return interpreter.compile("");
	}

}
