package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;

import org.gamestart.minecraft.modding.base.problemhandlers.PythonProblemHandler;
import org.gamestart.minecraft.modding.minecraft_python_programming.IArcane;
import org.gamestart.minecraft.modding.minecraft_python_programming.MagicItemFactory;
import org.gamestart.minecraft.modding.minecraft_python_programming.MinecraftPythonProgrammingMod;
import org.gamestart.minecraft.modding.minecraft_python_programming.RudimentaryMagicItem;

public class MPOnlyScriptLoader {

	private RudimentaryMagicItem magicVessel;

	private static MPOnlyScriptLoader SINGLETON;

	public static MPOnlyScriptLoader SINGLETON() {
		if (SINGLETON == null)
			SINGLETON = new MPOnlyScriptLoader("your_code_here");
		return SINGLETON;
	}

	private MPOnlyScriptLoader(String scriptFileName) {
		File scriptsDirectory = new File(
				ScriptLoaderConstants.MINECRAFT_PROGRAMMING_PATH);
		if (scriptsDirectory.canRead() && scriptsDirectory.isDirectory()) {
			try {
				File script = new File(
						ScriptLoaderConstants.MINECRAFT_PROGRAMMING_PATH + "\\"
								+ scriptFileName
								+ ScriptLoaderConstants.PYTHON_SCRIPT_EXTENSION);
				magicVessel = MagicItemFactory.create(
						SpellFactory.createNonCachingSpell(script),
						MinecraftPythonProgrammingMod.SCRIPT_RUN_COOLDOWN);
			} catch (Exception e) {
				PythonProblemHandler.printErrorMessageToDialogBox(e);
			}
		}
	}

	public IArcane getMagicVessel() {
		return magicVessel;
	}

}
