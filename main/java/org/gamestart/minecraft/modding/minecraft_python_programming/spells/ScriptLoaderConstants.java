package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

public class ScriptLoaderConstants {

	public static final String RESOURCES_PATH_KEY = "RESOURCES_PATH";
	public static final String RESOURCES_PATH = System.getProperty(RESOURCES_PATH_KEY);
	public static final String PYTHON_SCRIPT_EXTENSION = ".py";
	public static final String SCRIPTS_PATH = RESOURCES_PATH+"\\scripts";
	public static final String MINECRAFT_PROGRAMMING_PATH = SCRIPTS_PATH
			+ "\\mp";
	public static final String SPELLCRAFTERS_SCRIPTS_PATH = SCRIPTS_PATH
			+ "\\spellcrafters";
	public static final String SCAVENGER_HUNT_SCRIPTS_PATH = SCRIPTS_PATH
			+ "\\scavengerhunt";
	public static final String WAND_TEXTURE_LOCATION = RESOURCES_PATH + "\\assets\\spellcrafters\\textures\\items\\";
	public static final String WAND_TEXTURE_NAME_SUFFIX = "Wand";

	public static boolean isAPythonScript(File script) {
		return FilenameUtils.getExtension(script.getAbsolutePath()).matches(
				"[Pp][Yy][Cc]?");
	}
	
	public static boolean resourcePathExists() {
		return System.getProperty(ScriptLoaderConstants.RESOURCES_PATH_KEY) != null;
	}

}
