package org.gamestart.minecraft.modding.minecraft_python_programming.spells;


public class SpellCastingRunnable implements Runnable {

	private ISpell spell;
	private SpellInterpreter spellInterpreter;

	public SpellCastingRunnable(ISpell spell, SpellInterpreter spellInterpreter){
		this.spell = spell;
		this.spellInterpreter = spellInterpreter;
	}
	
	public void run() {
		boolean failure = !(spellInterpreter.interpretSpell(spell));
    }
}
