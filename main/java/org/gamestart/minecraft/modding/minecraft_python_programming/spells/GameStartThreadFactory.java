package org.gamestart.minecraft.modding.minecraft_python_programming.spells;


public class GameStartThreadFactory {

	public static Thread makeSpellThread(ISpell spell,
			SpellInterpreter interpreter) {
		return new Thread(new SpellCastingRunnable(spell, interpreter));
	}

	public static Thread makeJavaGameLoopThread() {
		return new Thread(new JavaGameLoopRunnable(new SpellInterpreter()));
	}
}
