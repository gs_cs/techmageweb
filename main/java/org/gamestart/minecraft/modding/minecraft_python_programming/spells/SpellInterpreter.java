package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;

import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

import org.gamestart.minecraft.modding.base.JavaFileIOHelper;
import org.gamestart.minecraft.modding.base.problemhandlers.JavaProblemHandler;
import org.gamestart.minecraft.modding.base.problemhandlers.PythonProblemHandler;
import org.python.util.PythonInterpreter;

public class SpellInterpreter {

	private PythonInterpreter interpreter;

	String scriptBasePath = "C:\\forge-1.7.2\\src\\main\\resources\\scripts\\base\\";
	String blockScriptPath = scriptBasePath + "blocks.py";
	String entitiesScriptPath = scriptBasePath + "entities.py";
	String itemScriptPath = scriptBasePath + "items.py";
	String baseSpellPath = scriptBasePath + "baseSpell.py";
	String particleScriptPath = scriptBasePath + "particles.py";
	String colorsScriptPath = scriptBasePath + "colors.py";

	public SpellInterpreter() {
		this.interpreter = new PythonInterpreter();
		String[] paths = new String[] { colorsScriptPath, blockScriptPath,
				entitiesScriptPath, itemScriptPath, particleScriptPath,
				baseSpellPath };
		for (String path : paths) {
			if (!executePythonFile(path)) {
				PythonProblemHandler
						.printErrorMessageToDialogBox(new Exception(
								"Could not add necessary base script at "
										+ path
										+ " to the Jython interpreter's consciousness."));
			}

		}
	}

	private boolean executePythonFile(String pythonFilePath) {
		synchronized (interpreter) {
			try {
				String textContentOfFile = JavaFileIOHelper.SINGLETON
						.getTextContentOfFile(new File(pythonFilePath));
				interpreter.exec(textContentOfFile);
			} catch (Exception e) {
				PythonProblemHandler.printErrorMessageToDialogBox(e);
				return false;
			}
			return true;
		}
	}

	public boolean interpretSpell(ISpell spell) {
		synchronized (interpreter) {
			try {
				interpreter.exec(spell.getCompiledPythonCode(interpreter));
			} catch (Exception e) {
				PythonProblemHandler.printErrorMessageToDialogBox(e);
				return false;
			}
			return true;
		}
	}

	public boolean interpretPython(String python) {
		synchronized (interpreter) {
			try {
				interpreter.exec(python);
			} catch (Exception e) {
				PythonProblemHandler.printErrorMessageToDialogBox(e);
				return false;
			}
			return true;
		}
	}

	public void setupImpactVariablesInPython(Vec3 positionOfImpact) {
		synchronized (interpreter) {
			if ((positionOfImpact != null)) {
				try {
					this.interpreter.set("impact_x", positionOfImpact.xCoord);
					this.interpreter.set("impact_y", positionOfImpact.yCoord);
					this.interpreter.set("impact_z", positionOfImpact.zCoord);
				} catch (Exception e) {
					PythonProblemHandler.printErrorMessageToDialogBox(e);
				}
			} else {
				JavaProblemHandler
						.printErrorMessageToDialogBox(new Exception(
								"Problem setting impact position variables to provided vector."));
			}
		}
	}

	public void setupRayVariablesInPython(MovingObjectPosition rayTrace) {
		synchronized (interpreter) {
			if ((rayTrace != null)) {
				try {
					this.interpreter.set("ray_x", rayTrace.hitVec.xCoord);
					this.interpreter.set("ray_y", rayTrace.hitVec.yCoord);
					this.interpreter.set("ray_z", rayTrace.hitVec.zCoord);
				} catch (Exception e) {
					PythonProblemHandler.printErrorMessageToDialogBox(e);
				}
			} else {
				JavaProblemHandler
						.printErrorMessageToDialogBox(new Exception(
								"Problem casting ray to get player's looked-at block."));
			}
		}
	}
}