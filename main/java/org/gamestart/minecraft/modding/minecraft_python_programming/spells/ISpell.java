package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;

import org.python.core.PyCode;
import org.python.util.PythonInterpreter;


public interface ISpell {
	public String getPythonScriptAsString();

	public File getPythonScriptAsFile();

	public PyCode getCompiledPythonCode(PythonInterpreter interpreter);

	String getSpellShortName();

	public int getRequiredExperienceLevel();
	
	public String getAuthorName();
	
	public String getSpellDisplayName();
}
