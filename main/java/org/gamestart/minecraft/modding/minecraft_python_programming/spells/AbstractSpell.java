package org.gamestart.minecraft.modding.minecraft_python_programming.spells;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gamestart.minecraft.modding.base.JavaFileIOHelper;
import org.gamestart.minecraft.modding.base.problemhandlers.JavaProblemHandler;
import org.python.core.PyCode;
import org.python.util.PythonInterpreter;

public abstract class AbstractSpell implements ISpell {
	private String PAIR_DEFINITION_GLYPH = "<=>";
	private String KEY_REQUIRED_EXPERIENCE_LEVEL = "level";
	private String KEY_AUTHOR_NAME = "author";
	private String NONE = "NONE";
	protected String name;
	protected File pythonScript;
	protected PyCode pythonCompiledCode;
	private Map<String, String> spellMetadata;
	private String KEY_SPELL_NAME = "name";

	@Override
	public abstract PyCode getCompiledPythonCode(PythonInterpreter interpreter);

	public AbstractSpell(String name, File pythonScript){
		this.name = name;
		this.pythonScript = pythonScript;
		this.spellMetadata = this.readMetadata();
	}

	protected void compileSpell(PythonInterpreter interpreter) {
		pythonCompiledCode = interpreter
				.compile(this.getPythonScriptAsString());
	}

	@Override
	public String getPythonScriptAsString() {
		return JavaFileIOHelper.SINGLETON.getTextContentOfFile(pythonScript);
	}

	@Override
	public File getPythonScriptAsFile() {
		return pythonScript;
	}

	@Override
	public String getSpellShortName() {
		return name;
	}

	@Override
	public int getRequiredExperienceLevel() {
		String value = getMetadataValueOrNONEIfNotPresent(KEY_REQUIRED_EXPERIENCE_LEVEL);
		if (value.equals(NONE)) {
			return 0;
		}
		int valueAsInt = 0;
		try {
			valueAsInt = Integer.parseInt(value);
		} catch (Exception e) {
			JavaProblemHandler.printErrorMessageToDialogBox(e);
		}
		return valueAsInt;
	}
	
	@Override
	public String getSpellDisplayName(){
		return getMetadataValueOrNONEIfNotPresent(KEY_SPELL_NAME);
	}

	@Override
	public String getAuthorName() {
		String value = getMetadataValueOrNONEIfNotPresent(KEY_AUTHOR_NAME);
		if(value.equals(NONE)){
			return "Anonymous";
		}
		return value;

	}

	private String getMetadataValueOrNONEIfNotPresent(String key) {
		String value = spellMetadata.get(key);
		if (value != null) {
			return value;
		}
		return NONE;
	}

	private Map<String, String> readMetadata() {
		Map<String, String> metadataMap = new LinkedHashMap<String, String>();
		String pythonScriptAsString = getPythonScriptAsString();
		String[] linesWithWhitespace = pythonScriptAsString.split(System
				.lineSeparator());

		for (String lineWithWhitespace : linesWithWhitespace) {
			String line = lineWithWhitespace.trim();

			boolean lineIsComment = line.startsWith("#");
			boolean lineIsKeyValueDefinition = line
					.contains(PAIR_DEFINITION_GLYPH);

			if (lineIsComment && lineIsKeyValueDefinition) {
				int delimiterIndex = line.indexOf(PAIR_DEFINITION_GLYPH);

				String key = line.substring(1, delimiterIndex).trim().toLowerCase();

				int startOfValue = PAIR_DEFINITION_GLYPH.length();
				String value = line.substring(delimiterIndex + startOfValue)
						.trim();

				metadataMap.put(key, value);
			}

		}
		return metadataMap;
	}

}
