package org.gamestart.minecraft.modding.minecraft_python_programming;

import java.io.File;

import org.gamestart.minecraft.modding.base.JavaFileIOHelper;
import org.gamestart.minecraft.modding.minecraft_python_programming.spells.SpellInterpreter;

public class PythonGameLoopRunnable implements Runnable {

	private SpellInterpreter gameLoopInterpreter;
	private File gameLoopScript;

	public PythonGameLoopRunnable(SpellInterpreter gameLoopInterpreter,
			File gameLoopScript) {
		this.gameLoopInterpreter = gameLoopInterpreter;
		this.gameLoopScript = gameLoopScript;
	}

	@Override
	public void run() {
		gameLoopInterpreter.interpretPython(JavaFileIOHelper.SINGLETON
				.getTextContentOfFile(gameLoopScript));
	}
}
