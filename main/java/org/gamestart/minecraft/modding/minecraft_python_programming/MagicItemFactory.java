package org.gamestart.minecraft.modding.minecraft_python_programming;

import org.gamestart.minecraft.modding.minecraft_python_programming.spells.ISpell;

public class MagicItemFactory {

	public static RudimentaryMagicItem create(ISpell boundSpell,
			int cooldownInMillis) {
		return new RudimentaryMagicItem(boundSpell, cooldownInMillis);
	}
}
