package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.block.Block;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

import org.gamestart.minecraft.modding.base.BlockFinder;

public class CommandGameStartCube extends CommandServerGameStart {

	private int x;
	private int y;
	private int z;
	private String blockType;
	private int size;

	public CommandGameStartCube(int x, int y, int z, String blockType, int size) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.blockType = blockType;
		this.size = size;
	}

	public CommandGameStartCube(double x, double y, double z, String blockType,
			int size) {
		this((int) x, (int) y, (int) z, blockType, size);
	}

	@Override
	public void doWork() {
		WorldServer worldserver = MinecraftServer.getServer()
				.worldServerForDimension(0);// TODO
		Block blocky;
		blocky = BlockFinder.getBlockWithName(blockType);
		if (blocky != null) {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					for (int k = 0; k < size; k++) {
						worldserver.setBlock(x + i, y + j, z + k, blocky, 0, 3);
					}
				}
			}
		}
	}
}
