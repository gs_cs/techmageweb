package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;

public class GameStartServerTickHandler {
	@SubscribeEvent
	public void onServerTick(ServerTickEvent event){
		if(event.phase.compareTo(Phase.END) == 0){
			GameStartCommandQueueServerSide.SINGLETON().runAndClearScheduledCommands();
		}
	}
}
