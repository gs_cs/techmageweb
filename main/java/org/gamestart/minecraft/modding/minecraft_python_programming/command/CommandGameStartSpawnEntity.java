package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

public class CommandGameStartSpawnEntity extends CommandServerGameStart {
	public double x;
	public double y;
	public double z;
	public String nameOfEntityToSpawn;

	public CommandGameStartSpawnEntity(double x, double y, double z,
			String entityName) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.nameOfEntityToSpawn = entityName;
	}

	public CommandGameStartSpawnEntity(int x, int y, int z, String name) {
		this((double) x, (double) y, (double) z, name);
	}

	public CommandGameStartSpawnEntity(String[] commandAndArgsToDeserialize) {
		this(Double.parseDouble(commandAndArgsToDeserialize[1]), Double
				.parseDouble(commandAndArgsToDeserialize[2]), Double
				.parseDouble(commandAndArgsToDeserialize[3]),
				commandAndArgsToDeserialize[4]);
	}

	public void doWork() {
		WorldServer worldserver = MinecraftServer.getServer()
				.worldServerForDimension(0);// TODO ONLY WORKS IN OVERWORLD FOR
											// NOW
		Entity entity = EntityLookup.getEntityByName(this.nameOfEntityToSpawn,
				worldserver);
		entity.setPositionAndRotation(x, y, z, 0, 0);

		// Tamed horse hack
		if (entity instanceof EntityHorse) {
			EntityHorse horse = (EntityHorse) entity;
			horse.setHorseTamed(true);
			horse.setHorseSaddled(true);
		}
		worldserver.spawnEntityInWorld(entity);
	}

}
