package org.gamestart.minecraft.modding.minecraft_python_programming.command;

public abstract class CommandServerGameStart extends CommandGameStartAbstract {
	public void execute() {
		GameStartCommandQueueServerSide.SINGLETON().scheduleCommand(this);
	}

}
