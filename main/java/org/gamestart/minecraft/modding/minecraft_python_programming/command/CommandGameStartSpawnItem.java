package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

public class CommandGameStartSpawnItem extends CommandServerGameStart {
	private double x;
	public double y;
	private double z;
	private String name;
	private int numberOfItemsToSpawn;

	public CommandGameStartSpawnItem(double x, double y, double z, String name,
			int numberOfItemsToSpawn) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.name = name;
		this.numberOfItemsToSpawn = numberOfItemsToSpawn;
	}

	public CommandGameStartSpawnItem(int x, int y, int z, String name,
			int numberOfItemsToSpawn) {
		this((double) x, (double) y, (double) z, name, numberOfItemsToSpawn);
	}

	public void doWork() {
		WorldServer worldserver = MinecraftServer.getServer()
				.worldServerForDimension(0);// TODO ONLY WORKS IN OVERWORLD FOR
											// NOW
		Item item = ItemLookup.getItemByName(name, worldserver);
		EntityItem entityWrapperForTheItemWithoutAHandToHoldIt = new EntityItem(
				worldserver, x, y, z);
		entityWrapperForTheItemWithoutAHandToHoldIt
				.setEntityItemStack(new ItemStack(item, numberOfItemsToSpawn));
		worldserver
				.spawnEntityInWorld(entityWrapperForTheItemWithoutAHandToHoldIt);
	}

}
