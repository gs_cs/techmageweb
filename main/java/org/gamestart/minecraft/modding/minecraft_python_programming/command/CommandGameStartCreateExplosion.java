package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

public class CommandGameStartCreateExplosion extends CommandServerGameStart {

	public int x;
	public int y;
	public int z;
	public int size;

	public CommandGameStartCreateExplosion(int x, int y, int z, int size) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.size = size;
	}

	public CommandGameStartCreateExplosion(double x, double y, double z,
			double size) {
		this((int) x, (int) y, (int) z, (int) size);
	}

	public CommandGameStartCreateExplosion(String[] commandAndArgs) {
		this(Double.parseDouble(commandAndArgs[1]), Double
				.parseDouble(commandAndArgs[2]), Double
				.parseDouble(commandAndArgs[3]), Double
				.parseDouble(commandAndArgs[4]));
	}

	public void doWork() {
		WorldServer worldserver = MinecraftServer.getServer()
				.worldServerForDimension(0); // TODO ONLY WORKS IN OVERWORLD FOR
												// NOW
		worldserver.createExplosion(new Entity(worldserver) {
			@Override
			protected void entityInit() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void readEntityFromNBT(NBTTagCompound var1) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void writeEntityToNBT(NBTTagCompound var1) {
				// TODO Auto-generated method stub

			}
		}, x, y, z, size, true);
	}

}