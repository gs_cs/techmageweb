package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.command.ICommand;

public interface ICommandGameStart extends ICommand {
	void doWork();
	void execute();
}
