package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

public class CommandGameStartGetPlayerPosition {
	
	int[] execute(){
		
		String username = MinecraftServer.getServer().getAllUsernames()[0];
		
		WorldServer world = MinecraftServer.getServer()
				.worldServerForDimension(0);
		List<EntityPlayerMP> players = new ArrayList<EntityPlayerMP>(
				(List<EntityPlayerMP>) world.playerEntities);
		
		for (EntityPlayerMP entityPlayerMP : players) {
			if (entityPlayerMP.getDisplayName().equals(username)) {
				return new int[]{
						(int)Math.round(entityPlayerMP.posX),
						(int)Math.round(entityPlayerMP.posY),
						(int)Math.round(entityPlayerMP.posZ)
				};
			}
		}
		
		return new int[]{0,70,0};
	}
	
}
