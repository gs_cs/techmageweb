package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

public class CommandGameStartTeleport extends CommandServerGameStart {

	public double x;
	public double y;
	public double z;
	public String teleportingPlayer;

	public CommandGameStartTeleport(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.teleportingPlayer = MinecraftServer.getServer().getAllUsernames()[0]; // for
																					// now,
																					// teleport
																					// to
																					// the
																					// first
																					// player
																					// we
																					// can
																					// find
		System.out.println("Command created to teleport "
				+ this.teleportingPlayer + " to " + x + " " + y + " " + z);
	}

	public CommandGameStartTeleport(int x, int y, int z) {
		this((double) x, (double) y, (double) z);
	}

	public CommandGameStartTeleport(String[] commandAndArgsToDeserialize) {
		this(Double.parseDouble(commandAndArgsToDeserialize[1]), Double
				.parseDouble(commandAndArgsToDeserialize[2]), Double
				.parseDouble(commandAndArgsToDeserialize[3]));
	}

	public void doWork() {
		WorldServer world = MinecraftServer.getServer()
				.worldServerForDimension(0);
		List<EntityPlayerMP> players = new ArrayList<EntityPlayerMP>(
				(List<EntityPlayerMP>) world.playerEntities); // I have never
																// seen a
																// ConcurrentModException
																// with this guy
																// but why risk
																// it?
		for (EntityPlayerMP entityPlayerMP : players) {
			if (entityPlayerMP.getDisplayName().equals(this.teleportingPlayer)) {
				entityPlayerMP.playerNetServerHandler.setPlayerLocation(this.x,
						this.y, this.z, entityPlayerMP.rotationYaw,
						entityPlayerMP.rotationPitch);
				return;
			}
		}
	}

}