package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import java.util.ArrayList;

public class GameStartCommandQueueServerSide extends
		GameStartCommandQueueAbstract {

	private static GameStartCommandQueueServerSide SINGLETON;

	private GameStartCommandQueueServerSide() {
		scheduledCommands = new ArrayList<ICommandGameStart>();
	}

	public synchronized void scheduleCommand(CommandServerGameStart command) {
		this.scheduledCommands.add(command);
	}

	public static GameStartCommandQueueServerSide SINGLETON() {
		if (SINGLETON == null) {
			SINGLETON = new GameStartCommandQueueServerSide();
		}
		return SINGLETON;
	}

}
