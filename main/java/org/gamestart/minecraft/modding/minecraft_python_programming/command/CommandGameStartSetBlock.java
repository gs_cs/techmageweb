package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.block.Block;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

import org.gamestart.minecraft.modding.base.BlockFinder;

public class CommandGameStartSetBlock extends CommandServerGameStart {

	private int x;
	private int y;
	private int z;
	private String blockType;
	private int metadata;

	public CommandGameStartSetBlock(int x, int y, int z, String blockType,
			int metadata) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.blockType = blockType;
		this.metadata = metadata;
	}

	public CommandGameStartSetBlock(double x, double y, double z,
			String blockType, int metadata) {
		this((int) x, (int) y, (int) z, blockType, metadata);
	}

	public CommandGameStartSetBlock(String[] commandAndArgsToDeserialize) {
		this(Integer.parseInt(commandAndArgsToDeserialize[1]), Integer
				.parseInt(commandAndArgsToDeserialize[2]), Integer
				.parseInt(commandAndArgsToDeserialize[3]),
				commandAndArgsToDeserialize[4], Integer
						.parseInt(commandAndArgsToDeserialize[5]));
	}

	public void doWork() {
		WorldServer worldserver = MinecraftServer.getServer()
				.worldServerForDimension(0);// TODO
		Block blocky;
		blocky = BlockFinder.getBlockWithName(blockType);
		if (blocky != null) {
			worldserver.setBlock(x, y, z, blocky, metadata, 3);
		}
	}

}
