package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import java.util.ArrayList;
import java.util.List;

public abstract class GameStartCommandQueueAbstract {

	public static final long MAXIMUM_MILLIS_PER_TICK_TO_USE_ON_COMMANDS = 200;
	public static final boolean LIMIT_COMMAND_TIME_PER_TICK = false;
	protected boolean slowModeEnabled = false;
	protected List<ICommandGameStart> scheduledCommands;

	public synchronized void runAndClearScheduledCommands() {
		if (LIMIT_COMMAND_TIME_PER_TICK) {
			runSomeCommandsAndClearThoseThatWereRun();
		} else {
			runAndClearAllCommands();
		}
	}

	private void runAndClearAllCommands() {
		for (ICommandGameStart command : this.scheduledCommands) {
			command.doWork();
		}
		this.scheduledCommands.clear();

	}

	private void runSomeCommandsAndClearThoseThatWereRun() {
		long timeToStopCommandExecutionProcess = System.currentTimeMillis()
				+ MAXIMUM_MILLIS_PER_TICK_TO_USE_ON_COMMANDS;
		ArrayList<ICommandGameStart> copyOfCommandListToIterateOver = new ArrayList<ICommandGameStart>(
				this.scheduledCommands);

		for (ICommandGameStart scheduledCommand : copyOfCommandListToIterateOver) {
			scheduledCommand.doWork();
			this.scheduledCommands.remove(scheduledCommand);
			if (System.currentTimeMillis() > timeToStopCommandExecutionProcess) {
				break;
			}

		}
	}

	public void toggleSlowMode() {
		this.slowModeEnabled = true;
	}
}
