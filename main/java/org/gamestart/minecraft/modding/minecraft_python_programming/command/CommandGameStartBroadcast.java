package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;

public class CommandGameStartBroadcast extends CommandServerGameStart {

	public String toBroadcast;

	public CommandGameStartBroadcast(String toBroadcast) {
		this.toBroadcast = toBroadcast;
	}

	public CommandGameStartBroadcast(String[] commandAndArgsToDeserialize) {
		toBroadcast = commandAndArgsToDeserialize[1];
	}

	@Override
	public void doWork() {
		MinecraftServer.getServer().getConfigurationManager()
				.sendChatMsg(new ChatComponentText(this.toBroadcast));
	}

}
