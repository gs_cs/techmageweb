package org.gamestart.minecraft.modding.minecraft_python_programming.command;

import net.minecraft.client.Minecraft;
import cpw.mods.fml.common.registry.GameRegistry;


public class CommandGameStartGetBlock {

	private int x;
	private int y;
	private int z;

	public CommandGameStartGetBlock(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public CommandGameStartGetBlock(double x, double y, double z){
		this((int)x,(int)y,(int)z);
	}
	public String execute() {
		return GameRegistry.findUniqueIdentifierFor(Minecraft.getMinecraft().theWorld.getBlock(x, y, z)).name;
	}
}
