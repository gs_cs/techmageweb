package org.gamestart.minecraft.modding.minecraft_python_programming;

import org.gamestart.minecraft.modding.base.CommonProxy;
import org.gamestart.minecraft.modding.base.DedicatedServerProxy;
import org.gamestart.minecraft.modding.minecraft_python_programming.command.GameStartCommandQueueServerSide;
import org.gamestart.minecraft.modding.minecraft_python_programming.spells.GameStartThreadFactory;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = MinecraftPythonProgrammingMod.MODID, version = MinecraftPythonProgrammingMod.VERSION, name = MinecraftPythonProgrammingMod.MODID)
public class MinecraftPythonProgrammingMod {
	public static final String MODID = "minecraftpythonweb";
	public static final String VERSION = "Pre-release";
	public static final int SCRIPT_RUN_COOLDOWN = 1500;

	@EventHandler
	public void init(FMLInitializationEvent event) {
	}

	@Instance(value = MinecraftPythonProgrammingMod.MODID)
	public static MinecraftPythonProgrammingMod instance;

	@SidedProxy(clientSide = "org.gamestart.minecraft.modding.base.CombinedClientProxy", serverSide = "org.gamestart.minecraft.modding.base.DedicatedServerProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		GameStartCommandQueueServerSide.SINGLETON();// initialization
	}

	@EventHandler
	public void load(FMLInitializationEvent event) {
		proxy.registerRenderers();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		if (proxy instanceof DedicatedServerProxy) {
			GameStartThreadFactory.makeJavaGameLoopThread().start();
		}
	}
}