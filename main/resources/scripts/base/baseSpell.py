from org.gamestart.minecraft.modding.minecraft_python_programming import command

def _get_my_position():
	yell('hello')
	return command.CommandGameStartGetPlayerPosition().execute()

def myX():
	return _get_my_position()[0]

def myY():
	return _get_my_position()[1]

def myZ():
	return _get_my_position()[2]

def explosion(x, y, z, size):
	command.CommandGameStartCreateExplosion(x, y, z, size).execute()

def spawnitem(x, y, z, name = BOAT,
              numberOfItems = 1):  # DOES NOT WORK NOPE DON'T TELL THE KIDS THIS WORKS NOPE 'CAUSE IT DON'T
	command.CommandGameStartSpawnItem(x, y, z, name, numberOfItems)

def spawnentity(x, y, z, name = PIG):
	command.CommandGameStartSpawnEntity(x, y, z, name).execute()

def setblock(x, y, z, blocktype = DIRT, meta = 0):
	command.CommandGameStartSetBlock(x, y, z, blocktype, meta).execute()

def getblock(x, y, z):
	blockName = command.CommandGameStartGetBlock(x, y, z).execute()
	if ('water' in blockName):
		return WATER
	elif ('lava' in blockName):
		return LAVA
	return blockName

def teleport(x, y, z):
	command.CommandGameStartTeleport(x, y, z).execute()

def cube(x, y, z, blocktype = DIRT, size = 2):
	command.CommandGameStartCube(x,y,z,blocktype,size).execute()

def getrect(x, y, z, x2, y2,
            z2):  #since these names are less than cool, x,y,z, is one corner and x2,y2,z2 are the other corner of an imaginary a rectangle
	return [[[getblock(i, j, k) for k in range(z, z2)] for j in range(y, y2)] for i in range(x, x2)]

def setrect(x, y, z, listOfListOfListsOfBlockTypes):
	for i in range(len(listOfListOfListsOfBlockTypes) - 1):
		for j in range(len(listOfListOfListsOfBlockTypes[0]) - 1):
			for k in range(len(listOfListOfListsOfBlockTypes[0][0]) - 1):
				setblock(x + i, y + j, z + k, listOfListOfListsOfBlockTypes[i][j][k])

def yell(toYell):
	command.CommandGameStartBroadcast(str(toYell)).execute()
