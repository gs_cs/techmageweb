NOTE: Blockly is also bundled in the regular forge-1.7.2 repository. Initially it is present on the "web" branch. Soon it will be integrated into the primary "gamestartschool" branch.

DEV SETUP
$ git clone https://bitbucket.org/sapphon/techmageweb.git
Ctrl-Alt-Shift-Q			# updates local forge installation to latest
$ python gamestartdeploy.py		# copies techmageweb/main into forge-1.7.2/src/main

for blockly development, see blockly/gamestartreadme.txt

TESTING
Eclipse -> Run Server
Eclipse -> Run Client, Multiplayer, connect to server at Self (127.0.0.1)
Desktop/TechMageBlockly-Shortcut	# launches blockly in chrome