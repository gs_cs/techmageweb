import subprocess
import shutil
import os

srcmainpath = os.path.join('c:\\','forge-1.7.2','src','main')
blocklypath = 'TechMageBlockly-Shortcut.lnk'

print('shutil.rmtree(' + srcmainpath + ')')
shutil.rmtree(srcmainpath)

print('shutil.copytree(\'main\', ' + srcmainpath + ')')
shutil.copytree('main', srcmainpath)


home = os.path.expanduser('~')


print('shutil.copyfile(' + blocklypath + ', ' + os.path.join(home,'Desktop',blocklypath) + ')')
shutil.copyfile(blocklypath,os.path.join(home,'Desktop',blocklypath))

print "ok"
